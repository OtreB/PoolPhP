#!/usr/bin/php
<?php

$input = "a";

while($input)
{
	echo "Entrez un nombre: ";
	$input = trim(fgets(STDIN));
	if (is_numeric($input))
	{
		if($input % 2 == 0)
			echo "Le chiffre $input est Pair\n";
		else
			echo "Le chiffre $input est Impair\n";
	}
	else if (!$input)
		echo "^D";
	else
		echo "$input n'est pas un chiffre\n";
}
?>